import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Buscar from './views/Buscar.vue'
import Distribuidor from './views/Distribuidor.vue'
import Login from './views/Login.vue'
import Registro from './views/Registro.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/buscar',
      name: 'buscar',
      component: Buscar
    },
    {
      path: '/distribuidor',
      name: 'distribuidor',
      component: Distribuidor
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/registro',
      name: 'registro',
      component: Registro
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
